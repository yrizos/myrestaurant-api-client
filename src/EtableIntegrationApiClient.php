<?php

namespace myRestaurant;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class EtableIntegrationApiClient extends Client
{
    const DEFAULT_BASE_URI = 'https://api.myrestaurant.gr';
    const NAME             = 'yrizos/myrestaurant-api-client';
    const VERSION          = '0.1';
    const DEFAULT_LANGUAGE = 'el';

    public function __construct(array $config = [])
    {
        if (!isset($config['base_uri'])) {
            $config['base_uri'] = self::DEFAULT_BASE_URI;
        }

        $token    = isset($config['token']) ? $config['token'] : '';
        $language = isset($config['language']) ? $config['language'] : self::DEFAULT_LANGUAGE;

        $headers                    = isset($config['headers']) && is_array($config['headers']) ? $config['headers'] : [];
        $headers['User-Agent']      = self::getUserAgent();
        $headers['Authorization']   = 'Basic ' . $token;
        $headers['Accept']          = 'application/vnd.api.tml.v1+json';
        $headers['Accept-Language'] = $language;

        $config['headers'] = $headers;

        parent::__construct($config);
    }

    public static function getUserAgent()
    {
        return self::NAME . '/' . self::VERSION . ' (+https://gitlab.com/yrizos/myrestaurant-api-client)';
    }

    public static function getArrayResponse(ResponseInterface $response)
    {
        $response = json_decode($response->getBody(), true);

        return $response;
    }

    public function getActivityReport()
    {
        $response = $this->request('GET', '/etable/activity_report');

        return self::getArrayResponse($response);
    }

    public function cancelReservation(array $data)
    {
        $params = self::getReservationParams(
            $data,
            [
                'reservation_id',
                'cancelled_reason',
            ],
            [
                'status'            => 'cancelled',
                'cancelled_comment' => '',
            ]
        );

        $response = $this->request('POST', '/etable/reservation', ['form_params' => $params]);

        return self::getArrayResponse($response);
    }

    public function createReservation(array $data)
    {
        return $this->updateReservation($data);
    }

    public function updateReservation(array $data)
    {
        $params = self::getReservationParams(
            $data,
            [
                'reservation_id',
                'store_id',
                'user_id',
                'status',
                'reserv_date',
                'reserv_persons',
                'reserv_fullname',
                'reserv_identifier',
                'reserv_phone',
                'reserv_phone_code',
            ],
            [
                'reserv_tables'     => 1,
                'reserv_comment'    => '',
                'offers'            => '',
                'cancelled_reason'  => '',
                'cancelled_comment' => '',
            ]
        );

        $response = $this->request('POST', '/etable/reservation', ['form_params' => $params]);

        return self::getArrayResponse($response);
    }

    protected static function getReservationParams(array $data, array $required = [], array $defaults = [])
    {
        foreach ($defaults as $field => $value) {
            if (!isset($data[$field])) {
                $data[$field] = $value;
            }
        }

        foreach ($required as $field) {
            if (!isset($data[$field])) {
                throw new \InvalidArgumentException('Field ' . $field . ' is required.');
            }
        }

        return $data;
    }

}
